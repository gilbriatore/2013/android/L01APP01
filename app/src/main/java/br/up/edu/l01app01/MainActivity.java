package br.up.edu.l01app01;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void verificar(View v){

        //Pega a caixa de texto da tela
        EditText caixaDeTexto = (EditText) findViewById(R.id.txtValor);
        EditText caixaDeMensagem = (EditText) findViewById(R.id.txtMensagem);

        //Pega o valor da caixa de texto e converte para inteiro;
        int vlr = Integer.parseInt(caixaDeTexto.getText().toString());

        //Verifica se o valor é maior, menor ou igual a 10...
        if (vlr > 10){
            //Mostra mensagem que é maior do que 10
            caixaDeMensagem.setText("É maior que 10!");
        } else {
            //Mostra mensagem de que é menor ou igual a 10
            caixaDeMensagem.setText("É menor ou igual a 10!");
        }
    }
}